
package com.soaint.fogacoop.api.pse.commons.domains.wsdl;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para operationType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="operationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OperCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlagDBCR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OperDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "operationType", propOrder = {
    "operCode",
    "flagDBCR",
    "operDesc",
    "value"
})
public class OperationType {

    @XmlElement(name = "OperCode")
    protected String operCode;
    @XmlElement(name = "FlagDBCR")
    protected String flagDBCR;
    @XmlElement(name = "OperDesc")
    protected String operDesc;
    @XmlElement(name = "Value", required = true)
    protected BigDecimal value;

    /**
     * Obtiene el valor de la propiedad operCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperCode() {
        return operCode;
    }

    /**
     * Define el valor de la propiedad operCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperCode(String value) {
        this.operCode = value;
    }

    /**
     * Obtiene el valor de la propiedad flagDBCR.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagDBCR() {
        return flagDBCR;
    }

    /**
     * Define el valor de la propiedad flagDBCR.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagDBCR(String value) {
        this.flagDBCR = value;
    }

    /**
     * Obtiene el valor de la propiedad operDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperDesc() {
        return operDesc;
    }

    /**
     * Define el valor de la propiedad operDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperDesc(String value) {
        this.operDesc = value;
    }

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

}
