package com.soaint.fogacoop.api.pse;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import java.util.ArrayList;
import java.util.List;

public class JaxWsHandlerResolver implements HandlerResolver {


    @SuppressWarnings("rawtypes")
    @Override
    public List<Handler> getHandlerChain(PortInfo pi) {
        //System.out.println("Inicia getHandlerChain");
        List<Handler> hchain = new ArrayList<Handler>();
        hchain.add(new JaxWsLoggingHandler());
        //System.out.println("Finaliza getHandlerChain");
        return hchain;
    }
}
