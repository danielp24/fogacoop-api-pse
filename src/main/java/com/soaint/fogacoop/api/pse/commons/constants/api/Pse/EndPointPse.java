package com.soaint.fogacoop.api.pse.commons.constants.api.Pse;

public interface EndPointPse {

    String BASE = "/fogacoop/pse";
    String TRANSACTION_PAYMENTS = "/getTransactions";
    String CREATE_TRANSACTION = "/createTransaction";
    String TRANSACTION_PAYMENTS_RESUME = "/getResumeTransactions";

}
