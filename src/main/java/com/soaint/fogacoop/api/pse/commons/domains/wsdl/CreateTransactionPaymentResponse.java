
package com.soaint.fogacoop.api.pse.commons.domains.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="createTransactionPaymentResult" type="{http://www.avisortech.com/eCollectWebservices}createTransactionResponseType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createTransactionPaymentResult"
})
@XmlRootElement(name = "createTransactionPaymentResponse")
public class CreateTransactionPaymentResponse {

    protected CreateTransactionResponseType createTransactionPaymentResult;

    /**
     * Obtiene el valor de la propiedad createTransactionPaymentResult.
     * 
     * @return
     *     possible object is
     *     {@link CreateTransactionResponseType }
     *     
     */
    public CreateTransactionResponseType getCreateTransactionPaymentResult() {
        return createTransactionPaymentResult;
    }

    /**
     * Define el valor de la propiedad createTransactionPaymentResult.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateTransactionResponseType }
     *     
     */
    public void setCreateTransactionPaymentResult(CreateTransactionResponseType value) {
        this.createTransactionPaymentResult = value;
    }

}
