package com.soaint.fogacoop.api.pse.commons.enums;

public enum HttpStatusCode {

    CONTINUE(100, "Continue"),
    SWITCHINGPROTOCOLS(101, "SwitchingProtocols"),
    OK(200, "Ok"),
    CREATED(201, "Create"),
    ACCEPTED (202, "Accepted"),
    NONAUTHORITATIVEINFORMATION (203,"NonAuthoritativeInformation"),
    NOCONTENT (204, "NoContent"),
    RESETCONTENT (205, "ResetContent"),
    PARTIALCONTENT (206,"PartialContent"),
    MULTIPLECHOICES (300, "MultipleChoices"),
    AMBIGUOUS (300, "Ambiguous"),
    MOVEDPERMANENTLY (301,"MovedPermanently"),
    MOVED (301, "Moved"),
    FOUND (302, "Found"),
    REDIRECT (302, "Redirect"),
    SEEOTHER (303, "SeeOther"),
    REDIRECTMETHOD (303, "RedirectMethod"),
    NOTMODIFIED (304, "NotModified"),
    USEPROXY (305, "UseProxy"),
    UNUSED (306, "Unused"),
    TEMPORARYREDIRECT (307,"TemporaryRedirect"),
    REDIRECTKEEPVERB (307,"RedirectKeepVerb"),
    BADREQUEST (400, "BadRequest"),
    UNAUTHORIZED (401, "Unathorized"),
    PAYMENTREQUIRED (402, "PaymentRequired"),
    FORBIDDEN (403, "Forbidden"),
    NOTFOUND (404,"NotFound"),
    METHODNOTALLOWED (405, "MethodNotAllowed"),
    NOTACCEPTABLE (406, "NotAcceptable"),
    PROXYAUTHENTICATIONREQUIERED (407, "ProxyAuthenticationRequiered"),
    REQUESTTIMEOUT (408, "RequestTimeOut"),
    CONFLICT(409, "Conflic"),
    GONE (410, "Gone"),
    LENGTHREQUIRED (411, "LengthRequiered"),
    PRECONDITIONFAILED (412, "PreconditionFailed"),
    REQUESTENTITYTOOLARGUE(413, "RequestEntityTooLarge"),
    REQUESTURITOOLONG (414, "RequestUriTooLong"),
    UNSUPPORTEDMEDIATYPE(415, "UnsupportedMediaType"),
    REQUESTEDRANGENOTSATISFIABLE(416, "RequestedRangeNotSatisfiable"),
    EXPECTATIONFAILED (417,"ExpectationFailed"),
    UPGRADEREQUIRED (426,"UpgradeRequired"),
    INTERNALSERVERERROR (500, "InternalServerError"),
    NOTIMPLEMENTED (501, "NotImplemented"),
    BADGATWAY (502,"BadGatway"),
    SERVICEUNAVAILABLE (503, "ServiceUnAvailable"),
    GATEWAYTIMEOUT (504, "GatewayTimeOut"),
    HTTPVERSIONNOTSUPPORTED (505, "HttpVersionNotSupported");

    private Integer value;
    private String name;

    HttpStatusCode(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
