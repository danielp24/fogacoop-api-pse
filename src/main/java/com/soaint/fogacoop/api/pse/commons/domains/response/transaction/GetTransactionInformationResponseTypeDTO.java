package com.soaint.fogacoop.api.pse.commons.domains.response.transaction;

import com.soaint.fogacoop.api.pse.commons.domains.wsdl.AdditionalInfoType;
import com.soaint.fogacoop.api.pse.commons.domains.wsdl.ArrayOfString;
import com.soaint.fogacoop.api.pse.commons.domains.wsdl.OperationType;
import com.soaint.fogacoop.api.pse.commons.domains.wsdl.RetriesTicketIdType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetTransactionInformationResponseTypeDTO {

    protected String entityCode;
    protected String ticketId;
    protected String trazabilityCode;
    protected String tranState;
    protected String returnCode;
    protected BigDecimal transValue;
    protected BigDecimal transVatValue;
    protected String payCurrency;
    protected BigDecimal currencyRate;
    protected XMLGregorianCalendar bankProcessDate;
    protected String fiCode;
    protected String bankName;
    protected String paymentSystem;
    protected String transCycle;
    protected String invoice;
    protected List<String> referenceArray;
    protected ArrayOfString authReferenceArray;
    protected List<OperationType> operationArray;
    protected List<RetriesTicketIdType> retriesTicketId;
    protected List<AdditionalInfoType> additionalInfoArray;
    protected String srvCode;
}
