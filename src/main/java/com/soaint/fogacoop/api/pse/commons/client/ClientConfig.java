package com.soaint.fogacoop.api.pse.commons.client;

import com.soaint.fogacoop.api.pse.commons.domains.wsdl.ECollectWebservicesv4Soap;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClientConfig {

    @Value("${client.ticketagent.address}")
    private String address;

    @Bean(name = "collectProxy")
    public ECollectWebservicesv4Soap collectProxy() {
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        jaxWsProxyFactoryBean.setServiceClass(ECollectWebservicesv4Soap.class);
        jaxWsProxyFactoryBean.setAddress(address);
        return (ECollectWebservicesv4Soap) jaxWsProxyFactoryBean.create();
    }
}
