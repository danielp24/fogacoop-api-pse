
package com.soaint.fogacoop.api.pse.commons.domains.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AdditionalInfoType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AdditionalInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InfoCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InfoData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalInfoType", propOrder = {
    "infoCode",
    "infoData"
})
public class AdditionalInfoType {

    @XmlElement(name = "InfoCode")
    protected String infoCode;
    @XmlElement(name = "InfoData")
    protected String infoData;

    /**
     * Obtiene el valor de la propiedad infoCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoCode() {
        return infoCode;
    }

    /**
     * Define el valor de la propiedad infoCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoCode(String value) {
        this.infoCode = value;
    }

    /**
     * Obtiene el valor de la propiedad infoData.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoData() {
        return infoData;
    }

    /**
     * Define el valor de la propiedad infoData.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoData(String value) {
        this.infoData = value;
    }

}
