
package com.soaint.fogacoop.api.pse.commons.domains.wsdl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para getTransactionInformationResponseType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="getTransactionInformationResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TicketId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TrazabilityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TranState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReturnCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="TransVatValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="PayCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyRate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BankProcessDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="FICode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentSystem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransCycle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Invoice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferenceArray" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AuthReferenceArray" type="{http://www.avisortech.com/eCollectWebservices}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="OperationArray" type="{http://www.avisortech.com/eCollectWebservices}operationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RetriesTicketId" type="{http://www.avisortech.com/eCollectWebservices}retriesTicketIdType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AdditionalInfoArray" type="{http://www.avisortech.com/eCollectWebservices}AdditionalInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SrvCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getTransactionInformationResponseType", propOrder = {
    "entityCode",
    "ticketId",
    "trazabilityCode",
    "tranState",
    "returnCode",
    "transValue",
    "transVatValue",
    "payCurrency",
    "currencyRate",
    "bankProcessDate",
    "fiCode",
    "bankName",
    "paymentSystem",
    "transCycle",
    "invoice",
    "referenceArray",
    "authReferenceArray",
    "operationArray",
    "retriesTicketId",
    "additionalInfoArray",
    "srvCode"
})
public class GetTransactionInformationResponseType {

    @XmlElement(name = "EntityCode")
    protected String entityCode;
    @XmlElement(name = "TicketId")
    protected String ticketId;
    @XmlElement(name = "TrazabilityCode")
    protected String trazabilityCode;
    @XmlElement(name = "TranState")
    protected String tranState;
    @XmlElement(name = "ReturnCode")
    protected String returnCode;
    @XmlElement(name = "TransValue", required = true)
    protected BigDecimal transValue;
    @XmlElement(name = "TransVatValue", required = true)
    protected BigDecimal transVatValue;
    @XmlElement(name = "PayCurrency")
    protected String payCurrency;
    @XmlElement(name = "CurrencyRate", required = true)
    protected BigDecimal currencyRate;
    @XmlElement(name = "BankProcessDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar bankProcessDate;
    @XmlElement(name = "FICode")
    protected String fiCode;
    @XmlElement(name = "BankName")
    protected String bankName;
    @XmlElement(name = "PaymentSystem")
    protected String paymentSystem;
    @XmlElement(name = "TransCycle")
    protected String transCycle;
    @XmlElement(name = "Invoice")
    protected String invoice;
    @XmlElement(name = "ReferenceArray")
    protected List<String> referenceArray;
    @XmlElement(name = "AuthReferenceArray")
    protected ArrayOfString authReferenceArray;
    @XmlElement(name = "OperationArray")
    protected List<OperationType> operationArray;
    @XmlElement(name = "RetriesTicketId")
    protected List<RetriesTicketIdType> retriesTicketId;
    @XmlElement(name = "AdditionalInfoArray")
    protected List<AdditionalInfoType> additionalInfoArray;
    @XmlElement(name = "SrvCode")
    protected String srvCode;

    /**
     * Obtiene el valor de la propiedad entityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityCode() {
        return entityCode;
    }

    /**
     * Define el valor de la propiedad entityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityCode(String value) {
        this.entityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad ticketId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketId() {
        return ticketId;
    }

    /**
     * Define el valor de la propiedad ticketId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketId(String value) {
        this.ticketId = value;
    }

    /**
     * Obtiene el valor de la propiedad trazabilityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrazabilityCode() {
        return trazabilityCode;
    }

    /**
     * Define el valor de la propiedad trazabilityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrazabilityCode(String value) {
        this.trazabilityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad tranState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranState() {
        return tranState;
    }

    /**
     * Define el valor de la propiedad tranState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranState(String value) {
        this.tranState = value;
    }

    /**
     * Obtiene el valor de la propiedad returnCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnCode() {
        return returnCode;
    }

    /**
     * Define el valor de la propiedad returnCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnCode(String value) {
        this.returnCode = value;
    }

    /**
     * Obtiene el valor de la propiedad transValue.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTransValue() {
        return transValue;
    }

    /**
     * Define el valor de la propiedad transValue.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTransValue(BigDecimal value) {
        this.transValue = value;
    }

    /**
     * Obtiene el valor de la propiedad transVatValue.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTransVatValue() {
        return transVatValue;
    }

    /**
     * Define el valor de la propiedad transVatValue.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTransVatValue(BigDecimal value) {
        this.transVatValue = value;
    }

    /**
     * Obtiene el valor de la propiedad payCurrency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayCurrency() {
        return payCurrency;
    }

    /**
     * Define el valor de la propiedad payCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayCurrency(String value) {
        this.payCurrency = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyRate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCurrencyRate() {
        return currencyRate;
    }

    /**
     * Define el valor de la propiedad currencyRate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCurrencyRate(BigDecimal value) {
        this.currencyRate = value;
    }

    /**
     * Obtiene el valor de la propiedad bankProcessDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBankProcessDate() {
        return bankProcessDate;
    }

    /**
     * Define el valor de la propiedad bankProcessDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBankProcessDate(XMLGregorianCalendar value) {
        this.bankProcessDate = value;
    }

    /**
     * Obtiene el valor de la propiedad fiCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFICode() {
        return fiCode;
    }

    /**
     * Define el valor de la propiedad fiCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFICode(String value) {
        this.fiCode = value;
    }

    /**
     * Obtiene el valor de la propiedad bankName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * Define el valor de la propiedad bankName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankName(String value) {
        this.bankName = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentSystem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentSystem() {
        return paymentSystem;
    }

    /**
     * Define el valor de la propiedad paymentSystem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentSystem(String value) {
        this.paymentSystem = value;
    }

    /**
     * Obtiene el valor de la propiedad transCycle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransCycle() {
        return transCycle;
    }

    /**
     * Define el valor de la propiedad transCycle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransCycle(String value) {
        this.transCycle = value;
    }

    /**
     * Obtiene el valor de la propiedad invoice.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoice() {
        return invoice;
    }

    /**
     * Define el valor de la propiedad invoice.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoice(String value) {
        this.invoice = value;
    }

    /**
     * Gets the value of the referenceArray property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the referenceArray property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReferenceArray().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getReferenceArray() {
        if (referenceArray == null) {
            referenceArray = new ArrayList<String>();
        }
        return this.referenceArray;
    }

    /**
     * Obtiene el valor de la propiedad authReferenceArray.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getAuthReferenceArray() {
        return authReferenceArray;
    }

    /**
     * Define el valor de la propiedad authReferenceArray.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setAuthReferenceArray(ArrayOfString value) {
        this.authReferenceArray = value;
    }

    /**
     * Gets the value of the operationArray property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the operationArray property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOperationArray().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OperationType }
     * 
     * 
     */
    public List<OperationType> getOperationArray() {
        if (operationArray == null) {
            operationArray = new ArrayList<OperationType>();
        }
        return this.operationArray;
    }

    /**
     * Gets the value of the retriesTicketId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the retriesTicketId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRetriesTicketId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RetriesTicketIdType }
     * 
     * 
     */
    public List<RetriesTicketIdType> getRetriesTicketId() {
        if (retriesTicketId == null) {
            retriesTicketId = new ArrayList<RetriesTicketIdType>();
        }
        return this.retriesTicketId;
    }

    /**
     * Gets the value of the additionalInfoArray property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalInfoArray property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalInfoArray().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalInfoType }
     * 
     * 
     */
    public List<AdditionalInfoType> getAdditionalInfoArray() {
        if (additionalInfoArray == null) {
            additionalInfoArray = new ArrayList<AdditionalInfoType>();
        }
        return this.additionalInfoArray;
    }

    /**
     * Obtiene el valor de la propiedad srvCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrvCode() {
        return srvCode;
    }

    /**
     * Define el valor de la propiedad srvCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrvCode(String value) {
        this.srvCode = value;
    }

}
