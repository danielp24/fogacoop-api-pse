package com.soaint.fogacoop.api.pse.commons.endpoint;

import com.soaint.fogacoop.api.pse.utils.ECollectWebservicesv4SoapImpl;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

@Configuration
class EndpointConfigWS {

    @Autowired
    private Bus bus;

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, new ECollectWebservicesv4SoapImpl());
        endpoint.publish("/webservice");
        return endpoint;
    }
}
