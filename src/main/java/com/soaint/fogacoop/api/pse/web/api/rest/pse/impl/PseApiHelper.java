package com.soaint.fogacoop.api.pse.web.api.rest.pse.impl;

import com.soaint.fogacoop.api.pse.commons.domains.response.transaction.GetTransactionInformationResumeResponseTypeDTO;
import com.soaint.fogacoop.api.pse.commons.domains.wsdl.GetTransactionInformationResponseType;

public class PseApiHelper {
    public static void convertAllToResumeInfo(GetTransactionInformationResumeResponseTypeDTO response, GetTransactionInformationResponseType responseType) {
        if (responseType != null) {
            response.setBankName((responseType.getBankName() != null) ? responseType.getBankName() : "");
            response.setFiCode((responseType.getFICode() != null) ? responseType.getFICode() : "");
            response.setPayCurrency((responseType.getPayCurrency() != null) ? responseType.getPayCurrency() : "");
            response.setReturnCode((responseType.getReturnCode() != null) ? responseType.getReturnCode() : "");
            response.setTicketId((responseType.getTicketId() != null) ? responseType.getTicketId() : "");
            response.setTranState((responseType.getTranState() != null) ? responseType.getTranState() : "");
            response.setTransValue((responseType.getTransValue() != null) ? responseType.getTransValue().toString() : "");
            response.setTrazabilityCode((responseType.getTrazabilityCode() != null) ? responseType.getTrazabilityCode() : "");
        }
    }
}
