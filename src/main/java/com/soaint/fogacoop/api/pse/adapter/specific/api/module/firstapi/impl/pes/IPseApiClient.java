package com.soaint.fogacoop.api.pse.adapter.specific.api.module.firstapi.impl.pes;

import com.soaint.fogacoop.api.pse.commons.domains.wsdl.*;
import com.soaint.fogacoop.api.pse.commons.exception.webClient.WebClientException;


public interface IPseApiClient {

    CreateTransactionResponseType createTransactionPayment(CreateTransactionPayment request) throws WebClientException;
    GetTransactionInformationResponseType getTransactionPayment(GetTransactionInformationType request) throws WebClientException;


}
