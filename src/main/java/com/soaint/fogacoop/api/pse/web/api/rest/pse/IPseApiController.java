package com.soaint.fogacoop.api.pse.web.api.rest.pse;


import com.soaint.fogacoop.api.pse.commons.domains.wsdl.CreateTransactionType;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface IPseApiController {

    ResponseEntity createTransactionPaymentResponse(CreateTransactionType createTransactionType, HttpServletRequest request);

    ResponseEntity getTransactionInformationResponse(String entityCode, String ticketId, HttpServletRequest request);

    ResponseEntity getTransactionInformationResumeResponse(String entityCode, String ticketId, HttpServletRequest request);

}
