package com.soaint.fogacoop.api.pse.commons.client;

import com.soaint.fogacoop.api.pse.adapter.specific.api.module.firstapi.impl.pes.impl.PseApiClient;
import com.soaint.fogacoop.api.pse.commons.domains.wsdl.*;
import com.soaint.fogacoop.api.pse.commons.exception.webClient.WebClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ECollectClient {

    @Autowired
    private PseApiClient confi;

    public CreateTransactionResponseType createTransactionResponseType(CreateTransactionType createTransactionPayment) throws WebClientException{
            CreateTransactionPayment request = new CreateTransactionPayment();
            request.setRequest(createTransactionPayment);
            return confi.createTransactionPayment(request);
    }

    public GetTransactionInformationResponseType getTransactionInformationResponseType( GetTransactionInformationType request) throws WebClientException {
            return confi.getTransactionPayment(request);
    }

}
