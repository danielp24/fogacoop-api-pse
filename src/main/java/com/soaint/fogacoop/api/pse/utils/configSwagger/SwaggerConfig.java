package com.soaint.fogacoop.api.pse.utils.configSwagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2

public class SwaggerConfig {
    /**
     * Swagger Doc
     * @return
     */
    @Bean
    public Docket swagger() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("PSEServices")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.soaint.fogacoop.api.pse"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }

    /**
     * Aplication Info
     * @return
     */
    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "PSE REST API",
                "Esta API provee una capa para interactuar directamente con los servicios de PSE",
                "1.0.0",
                "Rest",
                new Contact("SOAINT","http://soaint.com/","soaint@email.com"),
                "2019 - Soaint",
                "http://soaint.com/",
                Collections.emptyList()
        );
    }
}


