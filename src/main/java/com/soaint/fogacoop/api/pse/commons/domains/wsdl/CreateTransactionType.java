
package com.soaint.fogacoop.api.pse.commons.domains.wsdl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para createTransactionType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="createTransactionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SrvCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="TransVatValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="SrvCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="URLResponse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="URLRedirect" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Sign" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SignFields" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferenceArray" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PaymentSystem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FICode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Invoice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PolicyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AuthReferenceArray" type="{http://www.avisortech.com/eCollectWebservices}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="ItemArray" type="{http://www.avisortech.com/eCollectWebservices}itemType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SubservicesArray" type="{http://www.avisortech.com/eCollectWebservices}subserviceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AdditionalInfoArray" type="{http://www.avisortech.com/eCollectWebservices}AdditionalInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createTransactionType", propOrder = {
    "entityCode",
    "srvCode",
    "transValue",
    "transVatValue",
    "srvCurrency",
    "urlResponse",
    "urlRedirect",
    "sign",
    "signFields",
    "referenceArray",
    "paymentSystem",
    "fiCode",
    "invoice",
    "policyCode",
    "authReferenceArray",
    "itemArray",
    "subservicesArray",
    "additionalInfoArray"
})
public class CreateTransactionType {

    @XmlElement(name = "EntityCode")
    protected String entityCode;
    @XmlElement(name = "SrvCode")
    protected String srvCode;
    @XmlElement(name = "TransValue", required = true)
    protected BigDecimal transValue;
    @XmlElement(name = "TransVatValue", required = true)
    protected BigDecimal transVatValue;
    @XmlElement(name = "SrvCurrency")
    protected String srvCurrency;
    @XmlElement(name = "UrlResponse")
    protected String urlResponse;
    @XmlElement(name = "UrlRedirect")
    protected String urlRedirect;
    @XmlElement(name = "Sign")
    protected String sign;
    @XmlElement(name = "SignFields")
    protected String signFields;
    @XmlElement(name = "ReferenceArray")
    protected List<String> referenceArray;
    @XmlElement(name = "PaymentSystem")
    protected String paymentSystem;
    @XmlElement(name = "FICode")
    protected String fiCode;
    @XmlElement(name = "Invoice")
    protected String invoice;
    @XmlElement(name = "PolicyCode")
    protected String policyCode;
    @XmlElement(name = "AuthReferenceArray")
    protected ArrayOfString authReferenceArray;
    @XmlElement(name = "ItemArray")
    protected List<ItemType> itemArray;
    @XmlElement(name = "SubservicesArray")
    protected List<SubserviceType> subservicesArray;
    @XmlElement(name = "AdditionalInfoArray")
    protected List<AdditionalInfoType> additionalInfoArray;

    /**
     * Obtiene el valor de la propiedad entityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityCode() {
        return entityCode;
    }

    /**
     * Define el valor de la propiedad entityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityCode(String value) {
        this.entityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad srvCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrvCode() {
        return srvCode;
    }

    /**
     * Define el valor de la propiedad srvCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrvCode(String value) {
        this.srvCode = value;
    }

    /**
     * Obtiene el valor de la propiedad transValue.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTransValue() {
        return transValue;
    }

    /**
     * Define el valor de la propiedad transValue.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTransValue(BigDecimal value) {
        this.transValue = value;
    }

    /**
     * Obtiene el valor de la propiedad transVatValue.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTransVatValue() {
        return transVatValue;
    }

    /**
     * Define el valor de la propiedad transVatValue.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTransVatValue(BigDecimal value) {
        this.transVatValue = value;
    }

    /**
     * Obtiene el valor de la propiedad srvCurrency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrvCurrency() {
        return srvCurrency;
    }

    /**
     * Define el valor de la propiedad srvCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrvCurrency(String value) {
        this.srvCurrency = value;
    }

    /**
     * Obtiene el valor de la propiedad urlResponse.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlResponse() {
        return urlResponse;
    }

    /**
     * Define el valor de la propiedad urlResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlResponse(String value) {
        this.urlResponse = value;
    }

    /**
     * Obtiene el valor de la propiedad urlRedirect.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlRedirect() {
        return urlRedirect;
    }

    /**
     * Define el valor de la propiedad urlRedirect.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlRedirect(String value) {
        this.urlRedirect = value;
    }

    /**
     * Obtiene el valor de la propiedad sign.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSign() {
        return sign;
    }

    /**
     * Define el valor de la propiedad sign.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSign(String value) {
        this.sign = value;
    }

    /**
     * Obtiene el valor de la propiedad signFields.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSignFields() {
        return signFields;
    }

    /**
     * Define el valor de la propiedad signFields.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSignFields(String value) {
        this.signFields = value;
    }

    /**
     * Gets the value of the referenceArray property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the referenceArray property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReferenceArray().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getReferenceArray() {
        if (referenceArray == null) {
            referenceArray = new ArrayList<String>();
        }
        return this.referenceArray;
    }

    /**
     * Obtiene el valor de la propiedad paymentSystem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentSystem() {
        return paymentSystem;
    }

    /**
     * Define el valor de la propiedad paymentSystem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentSystem(String value) {
        this.paymentSystem = value;
    }

    /**
     * Obtiene el valor de la propiedad fiCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFICode() {
        return fiCode;
    }

    /**
     * Define el valor de la propiedad fiCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFICode(String value) {
        this.fiCode = value;
    }

    /**
     * Obtiene el valor de la propiedad invoice.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoice() {
        return invoice;
    }

    /**
     * Define el valor de la propiedad invoice.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoice(String value) {
        this.invoice = value;
    }

    /**
     * Obtiene el valor de la propiedad policyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyCode() {
        return policyCode;
    }

    /**
     * Define el valor de la propiedad policyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyCode(String value) {
        this.policyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad authReferenceArray.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getAuthReferenceArray() {
        return authReferenceArray;
    }

    /**
     * Define el valor de la propiedad authReferenceArray.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setAuthReferenceArray(ArrayOfString value) {
        this.authReferenceArray = value;
    }

    /**
     * Gets the value of the itemArray property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemArray property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemArray().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemType }
     * 
     * 
     */
    public List<ItemType> getItemArray() {
        if (itemArray == null) {
            itemArray = new ArrayList<ItemType>();
        }
        return this.itemArray;
    }

    /**
     * Gets the value of the subservicesArray property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subservicesArray property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubservicesArray().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubserviceType }
     * 
     * 
     */
    public List<SubserviceType> getSubservicesArray() {
        if (subservicesArray == null) {
            subservicesArray = new ArrayList<SubserviceType>();
        }
        return this.subservicesArray;
    }

    /**
     * Gets the value of the additionalInfoArray property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalInfoArray property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalInfoArray().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalInfoType }
     * 
     * 
     */
    public List<AdditionalInfoType> getAdditionalInfoArray() {
        if (additionalInfoArray == null) {
            additionalInfoArray = new ArrayList<AdditionalInfoType>();
        }
        return this.additionalInfoArray;
    }

}
