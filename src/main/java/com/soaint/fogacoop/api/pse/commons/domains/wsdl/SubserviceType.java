
package com.soaint.fogacoop.api.pse.commons.domains.wsdl;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para subserviceType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="subserviceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SrvCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ValueType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="TransVatValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "subserviceType", propOrder = {
    "entityCode",
    "srvCode",
    "valueType",
    "transValue",
    "transVatValue"
})
public class SubserviceType {

    @XmlElement(name = "EntityCode")
    protected String entityCode;
    @XmlElement(name = "SrvCode")
    protected String srvCode;
    @XmlElement(name = "ValueType")
    protected String valueType;
    @XmlElement(name = "TransValue", required = true)
    protected BigDecimal transValue;
    @XmlElement(name = "TransVatValue", required = true)
    protected BigDecimal transVatValue;

    /**
     * Obtiene el valor de la propiedad entityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityCode() {
        return entityCode;
    }

    /**
     * Define el valor de la propiedad entityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityCode(String value) {
        this.entityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad srvCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrvCode() {
        return srvCode;
    }

    /**
     * Define el valor de la propiedad srvCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrvCode(String value) {
        this.srvCode = value;
    }

    /**
     * Obtiene el valor de la propiedad valueType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValueType() {
        return valueType;
    }

    /**
     * Define el valor de la propiedad valueType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValueType(String value) {
        this.valueType = value;
    }

    /**
     * Obtiene el valor de la propiedad transValue.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTransValue() {
        return transValue;
    }

    /**
     * Define el valor de la propiedad transValue.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTransValue(BigDecimal value) {
        this.transValue = value;
    }

    /**
     * Obtiene el valor de la propiedad transVatValue.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTransVatValue() {
        return transVatValue;
    }

    /**
     * Define el valor de la propiedad transVatValue.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTransVatValue(BigDecimal value) {
        this.transVatValue = value;
    }

}
