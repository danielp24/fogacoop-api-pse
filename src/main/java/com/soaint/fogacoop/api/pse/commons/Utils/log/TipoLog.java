package com.soaint.fogacoop.api.pse.commons.Utils.log;

public enum TipoLog {
    DEBUG, ERROR, FATAL, INFO, WARNING
}