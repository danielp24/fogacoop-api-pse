
package com.soaint.fogacoop.api.pse;

import com.soaint.fogacoop.api.pse.commons.Utils.log.LogUtils;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;


public class JaxWsLoggingHandler implements SOAPHandler<SOAPMessageContext> {

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext c) {
        boolean isOutboundMessage = (Boolean) c.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        if (isOutboundMessage) {

            SOAPMessage message = c.getMessage();
            try {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                message.writeTo(stream);
                LogUtils.info(new String(stream.toByteArray(), "utf-8"));
            } catch (SOAPException e) {
                System.out.println(e);
            } catch (IOException e) {
                System.out.println(e);
            }
        }

        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext c) {
        SOAPMessage message = c.getMessage();
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            message.writeTo(stream);
            LogUtils.info(new String(stream.toByteArray(), "utf-8"));
        } catch (SOAPException e) {
            LogUtils.error(e);
        } catch (IOException e) {
            LogUtils.error(e);
        }
        return true;
    }

    @Override
    public void close(MessageContext mc) {

    }

}
