package com.soaint.fogacoop.api.pse.commons.Utils;

public enum ResponseType {

    SUCCESS(0),
    CATCHED(1),
    EXCEPTION(2),
    NOSESSION(3);

    public int getType() {
        return type;
    }

    // Constructor
    ResponseType(int type) {
        this.type = type;
    }

    private int type;

}