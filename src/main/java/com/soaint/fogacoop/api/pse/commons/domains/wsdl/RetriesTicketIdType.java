
package com.soaint.fogacoop.api.pse.commons.domains.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para retriesTicketIdType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="retriesTicketIdType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TicketId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TranState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retriesTicketIdType", propOrder = {
    "ticketId",
    "tranState"
})
public class RetriesTicketIdType {

    @XmlElement(name = "TicketId")
    protected String ticketId;
    @XmlElement(name = "TranState")
    protected String tranState;

    /**
     * Obtiene el valor de la propiedad ticketId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicketId() {
        return ticketId;
    }

    /**
     * Define el valor de la propiedad ticketId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicketId(String value) {
        this.ticketId = value;
    }

    /**
     * Obtiene el valor de la propiedad tranState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranState() {
        return tranState;
    }

    /**
     * Define el valor de la propiedad tranState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranState(String value) {
        this.tranState = value;
    }

}
