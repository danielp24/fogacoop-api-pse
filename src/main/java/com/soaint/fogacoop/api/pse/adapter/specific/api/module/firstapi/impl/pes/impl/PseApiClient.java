package com.soaint.fogacoop.api.pse.adapter.specific.api.module.firstapi.impl.pes.impl;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.soaint.fogacoop.api.pse.JaxWsHandlerResolver;
import com.soaint.fogacoop.api.pse.adapter.manager.impl.EndpointManagerAbstract;
import com.soaint.fogacoop.api.pse.adapter.specific.api.module.firstapi.impl.pes.IPseApiClient;
import com.soaint.fogacoop.api.pse.adapter.specific.infrastructure.EndpointConfig;
import com.soaint.fogacoop.api.pse.commons.Utils.log.LogUtils;
import com.soaint.fogacoop.api.pse.commons.domains.wsdl.*;
import com.soaint.fogacoop.api.pse.commons.exception.webClient.WebClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.xml.ws.soap.AddressingFeature;
import java.net.MalformedURLException;
import java.net.URL;

@Component
@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.NON_PRIVATE)
public class PseApiClient extends EndpointManagerAbstract implements IPseApiClient {


    private EndpointConfig endpointConfig;

    @Autowired
    public PseApiClient(EndpointConfig endpointConfig) {
        this.endpointConfig = endpointConfig;
    }


    @Value("${url.wsdl.interno}")
    private String WSDL_INTERNO;

    @Value("${url.wsdl.externo}")
    private String WSDL_EXTERNO;

    @Value("${wsdl.pse.externo}")
    private String WSDL_PSE_EXTERNO;

    @Value("${protocol.wsdl.interno}")
    private String PROTOCOL_WSDL_INTERNO;


    @Override
    public CreateTransactionResponseType createTransactionPayment(CreateTransactionPayment request) throws WebClientException {

        CreateTransactionResponseType response = new CreateTransactionResponseType();

        try {
            String WSDL;
            if (Boolean.parseBoolean(WSDL_PSE_EXTERNO) == Boolean.TRUE) {
                WSDL = WSDL_EXTERNO;
            } else {
                WSDL = PROTOCOL_WSDL_INTERNO+WSDL_INTERNO;
            }
            LogUtils.info(" WSDL Use = "+WSDL);
            URL finalUrl = new URL(WSDL);
            ECollectWebservicesv4 service = new ECollectWebservicesv4(finalUrl);
            service.setHandlerResolver(new JaxWsHandlerResolver());

            ECollectWebservicesv4Soap port = service.getECollectWebservicesv4Soap(new AddressingFeature(false, false));

            response = port.createTransactionPayment(request.getRequest());
            LogUtils.info(response.getReturnCode());
        } catch (MalformedURLException ex) {
            LogUtils.error("\n\n ERROR AL CREAR LOS PORTTYPE: " + ex.getMessage() + "  ----- FIN ERROR");
            LogUtils.error(ex);
            throw new WebClientException("1000","\n\n ERROR AL CREAR LOS PORTTYPE: " + ex.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            throw new WebClientException("1000",e.getMessage());
        }
        return response;
    }

    @Override
    public GetTransactionInformationResponseType getTransactionPayment(GetTransactionInformationType request) throws WebClientException {
        GetTransactionInformationResponseType response = new GetTransactionInformationResponseType();
        try {
            String WSDL;
            //ECollectWebservicesv4 service = new ECollectWebservicesv4();
            if (Boolean.parseBoolean(WSDL_PSE_EXTERNO) == Boolean.TRUE) {
                WSDL = WSDL_EXTERNO;
            } else {
                WSDL = PROTOCOL_WSDL_INTERNO+WSDL_INTERNO;
            }
            LogUtils.info(" WSDL Use = "+WSDL);

            URL finalUrl = new URL(WSDL);
            ECollectWebservicesv4 service = new ECollectWebservicesv4(finalUrl);
            service.setHandlerResolver(new JaxWsHandlerResolver());

            ECollectWebservicesv4Soap port = service.getECollectWebservicesv4Soap(new AddressingFeature(false, false));

            response = port.getTransactionInformation(request);
            LogUtils.info(response.getReturnCode());
        } catch (MalformedURLException ex) {
            LogUtils.error("\n\n ERROR AL CREAR LOS PORTTYPE: " + ex.getMessage() + "  ----- FIN ERROR");
            LogUtils.error(ex);
            throw new WebClientException("1000","\n\n ERROR AL CREAR LOS PORTTYPE: " + ex.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            throw new WebClientException("1000",e.getMessage());
        }
        return response;
    }
}
