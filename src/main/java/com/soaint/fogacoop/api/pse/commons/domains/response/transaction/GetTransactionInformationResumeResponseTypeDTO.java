package com.soaint.fogacoop.api.pse.commons.domains.response.transaction;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetTransactionInformationResumeResponseTypeDTO {

    protected String ticketId;
    protected String trazabilityCode;
    protected String tranState;
    protected String returnCode;
    protected String transValue;
    protected String payCurrency;
    protected String fiCode;
    protected String bankName;
}
