package com.soaint.fogacoop.api.pse.web.api.rest.pse.impl;

import com.soaint.fogacoop.api.pse.commons.Utils.exception.ValidationUtils;
import com.soaint.fogacoop.api.pse.commons.Utils.log.LogUtils;
import com.soaint.fogacoop.api.pse.commons.client.ECollectClient;
import com.soaint.fogacoop.api.pse.commons.constants.api.Pse.EndPointPse;
import com.soaint.fogacoop.api.pse.commons.domains.response.builder.ResponseBuilder;
import com.soaint.fogacoop.api.pse.commons.domains.response.transaction.GetTransactionInformationResumeResponseTypeDTO;
import com.soaint.fogacoop.api.pse.commons.domains.wsdl.CreateTransactionResponseType;
import com.soaint.fogacoop.api.pse.commons.domains.wsdl.CreateTransactionType;
import com.soaint.fogacoop.api.pse.commons.domains.wsdl.GetTransactionInformationResponseType;
import com.soaint.fogacoop.api.pse.commons.domains.wsdl.GetTransactionInformationType;
import com.soaint.fogacoop.api.pse.commons.enums.TransactionState;
import com.soaint.fogacoop.api.pse.commons.exception.webClient.WebClientException;
import com.soaint.fogacoop.api.pse.web.api.rest.pse.IPseApiController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = EndPointPse.BASE)
@CrossOrigin(origins = "*", methods = {RequestMethod.DELETE, RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
@Api(description = "Servicio que contiene las operaciones atadadas a la integración de operaciones con PSE")
public class PseApiController implements IPseApiController {

    private ECollectClient eCollectClient;

    @Autowired
    public PseApiController(ECollectClient eCollectClient) {
        this.eCollectClient = eCollectClient;
    }

    @PostMapping(value = EndPointPse.CREATE_TRANSACTION)
    @Override
    @ApiOperation("Retorna los datos de la creacion de la transaccción ante PSE")
    public ResponseEntity createTransactionPaymentResponse(@RequestBody CreateTransactionType createTransactionPayment, HttpServletRequest request) {
        ResponseBuilder response = ResponseBuilder.newBuilder();
        try {
            ValidationUtils.validateNullEmptyObject(createTransactionPayment);
            CreateTransactionResponseType result = this.eCollectClient.createTransactionResponseType(createTransactionPayment);
            response.withStatus(result.getReturnCode().equals("SUCCESS") ? HttpStatus.OK : HttpStatus.BAD_REQUEST)
                    .withMessage(result.getReturnCode().equals("SUCCESS") ? "Creado Exitosamente" : "Error creando Transacción")
                    .withPath(request.getRequestURI())
                    .withResponse(result)
                    .withTransactionState(result.getReturnCode().equals("SUCCESS") ? TransactionState.OK : TransactionState.FAIL);

        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }

    @GetMapping(value = EndPointPse.TRANSACTION_PAYMENTS)
    @Override
    @ApiOperation("Retorna los datos de la obtención de la transacción efectuada por TicketId")
    public ResponseEntity getTransactionInformationResponse(@RequestParam("entityCode") String entityCode, @RequestParam("ticketId") String ticketId, HttpServletRequest request) {
        ResponseBuilder response = ResponseBuilder.newBuilder();
        try {
            ValidationUtils.validateNullEmptyString(entityCode);
            ValidationUtils.validateNullEmptyString(ticketId);
            GetTransactionInformationType getTransactionInformationType = new GetTransactionInformationType();
            getTransactionInformationType.setEntityCode(entityCode);
            getTransactionInformationType.setTicketId(ticketId);
            GetTransactionInformationResponseType responseType = eCollectClient.getTransactionInformationResponseType(getTransactionInformationType);

            response.withStatus(responseType.getReturnCode().equals("SUCCESS") ? HttpStatus.OK : HttpStatus.BAD_REQUEST)
                    .withMessage(responseType.getReturnCode().equals("SUCCESS") ? "Datos Obtenidos" : "Eror Obteniendo Datos")
                    .withResponse(responseType)
                    .withTransactionState(responseType.getReturnCode().equals("SUCCESS") ? TransactionState.OK : TransactionState.FAIL);

        } catch (WebClientException e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            response.withStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return response
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }
    }

    @GetMapping(value = EndPointPse.TRANSACTION_PAYMENTS_RESUME)
    @Override
    @ApiOperation("Retorna los datos en resumen de la obtención de la transacción efectuada por TicketId")
    public ResponseEntity getTransactionInformationResumeResponse(@RequestParam("entityCode") String entityCode, @RequestParam("ticketId") String ticketId, HttpServletRequest request) {
        ResponseBuilder res = ResponseBuilder.newBuilder();
        try {
            ValidationUtils.validateNullEmptyString(entityCode);
            ValidationUtils.validateNullEmptyString(ticketId);
            GetTransactionInformationType getTransactionInformationType = new GetTransactionInformationType();
            getTransactionInformationType.setEntityCode(entityCode);
            getTransactionInformationType.setTicketId(ticketId);
            GetTransactionInformationResponseType responseType = eCollectClient.getTransactionInformationResponseType(getTransactionInformationType);
            GetTransactionInformationResumeResponseTypeDTO response = new GetTransactionInformationResumeResponseTypeDTO();
            PseApiHelper.convertAllToResumeInfo(response, responseType);

            res.withStatus(responseType.getReturnCode().equals("SUCCESS") ? HttpStatus.OK : HttpStatus.BAD_REQUEST)
                    .withMessage(responseType.getReturnCode().equals("SUCCESS") ? "Datos Obtenidos" : "Eror Obteniendo Datos")
                    .withResponse(response)
                    .withTransactionState(responseType.getReturnCode().equals("SUCCESS") ? TransactionState.OK : TransactionState.FAIL)
                    .buildResponse();

        } catch (WebClientException e) {
            LogUtils.error(e);
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(e.getMessage());
        } catch (Exception e) {
            LogUtils.error(e);
            res.withStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(e.getMessage())
                    .withTransactionState(TransactionState.FAIL);
        } finally {
            return res
                    .withPath(request.getRequestURI())
                    .buildResponse();
        }

    }
}

