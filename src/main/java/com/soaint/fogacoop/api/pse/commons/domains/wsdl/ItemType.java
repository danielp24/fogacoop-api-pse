
package com.soaint.fogacoop.api.pse.commons.domains.wsdl;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para itemType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="itemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlagDBCR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemQty" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ItemDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "itemType", propOrder = {
    "itemCode",
    "flagDBCR",
    "itemQty",
    "itemDesc",
    "value"
})
public class ItemType {

    @XmlElement(name = "ItemCode")
    protected String itemCode;
    @XmlElement(name = "FlagDBCR")
    protected String flagDBCR;
    @XmlElement(name = "ItemQty")
    protected int itemQty;
    @XmlElement(name = "ItemDesc")
    protected String itemDesc;
    @XmlElement(name = "Value", required = true)
    protected BigDecimal value;

    /**
     * Obtiene el valor de la propiedad itemCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * Define el valor de la propiedad itemCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemCode(String value) {
        this.itemCode = value;
    }

    /**
     * Obtiene el valor de la propiedad flagDBCR.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagDBCR() {
        return flagDBCR;
    }

    /**
     * Define el valor de la propiedad flagDBCR.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagDBCR(String value) {
        this.flagDBCR = value;
    }

    /**
     * Obtiene el valor de la propiedad itemQty.
     * 
     */
    public int getItemQty() {
        return itemQty;
    }

    /**
     * Define el valor de la propiedad itemQty.
     * 
     */
    public void setItemQty(int value) {
        this.itemQty = value;
    }

    /**
     * Obtiene el valor de la propiedad itemDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemDesc() {
        return itemDesc;
    }

    /**
     * Define el valor de la propiedad itemDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemDesc(String value) {
        this.itemDesc = value;
    }

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

}
