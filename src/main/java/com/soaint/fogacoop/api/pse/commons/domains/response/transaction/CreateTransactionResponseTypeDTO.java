package com.soaint.fogacoop.api.pse.commons.domains.response.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateTransactionResponseTypeDTO {

    protected String returnCode;
    protected String ticketId;
    protected String eCollectUrl;
}
