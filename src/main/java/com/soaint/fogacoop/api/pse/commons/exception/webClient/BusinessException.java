package com.soaint.fogacoop.api.pse.commons.exception.webClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BusinessException {

    //Generic

    //When code sive is empty
    public static String codeSizeIsEmpty;
    public static String valueSizeIsEmpty;

    //When catch parameter empty
    public static String codeParametertIsEmpty;
    public static String valueParametertIsEmpty;

    //When catch all exception
    public static String codeCatchAllException;



    //set All Business Exceptions
    @Autowired
    private void setBusinessExceptions(
            @Value("${business.exception.codeSizeIsEmpty}") String cSizeIsEmpty,
            @Value("${business.exception.valueSizeIsEmpty}") String vSizeIsEmpty,
            @Value("${business.exception.codeCatchAllException}") String cCatchAllException,
            @Value("${business.exception.codeParametertIsEmpty}") String cParametertIsEmpty,
            @Value("${business.exception.valueParametertIsEmpty}") String vParametertIsEmpty

    ) {
        codeSizeIsEmpty = cSizeIsEmpty;
        valueSizeIsEmpty = vSizeIsEmpty;
        codeCatchAllException = cCatchAllException;

        //Params Validator
        codeParametertIsEmpty = cParametertIsEmpty;
        valueParametertIsEmpty = vParametertIsEmpty;
    }


}
