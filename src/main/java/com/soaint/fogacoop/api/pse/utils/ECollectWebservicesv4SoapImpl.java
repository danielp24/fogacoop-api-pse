package com.soaint.fogacoop.api.pse.utils;

import com.soaint.fogacoop.api.pse.commons.domains.wsdl.*;

public class ECollectWebservicesv4SoapImpl implements ECollectWebservicesv4Soap {


    @Override
    public CreateTransactionResponseType createTransactionPayment(CreateTransactionType request) {

        ObjectFactory factory = new ObjectFactory();
        CreateTransactionResponseType response = factory.createCreateTransactionResponseType();

        return response;
    }

    @Override
    public GetTransactionInformationResponseType getTransactionInformation(GetTransactionInformationType request) {
        ObjectFactory factory = new ObjectFactory();
        GetTransactionInformationResponseType response = factory.createGetTransactionInformationResponseType();

        return response;
    }

}
